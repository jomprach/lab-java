
public class lab13 {
	
	public static void main(String [] args) {
		int [][] twoD_Array = {
				{1, 2, 3},{4, 5, 6, 7},{8, 9}
		};
		for (int [] row : twoD_Array) {
			for (int element : row) {
				System.out.println (element);
				}
		}
		
		int[] firstRow = twoD_Array[0];
		int[] secondRow = twoD_Array[2];
		int[] lastRow = twoD_Array[twoD_Array.length - 1];
		
		int lastItemInFirstRow = firstRow[firstRow.length - 1];
		int lastItemInSecondRow = firstRow[secondRow.length - 1];
		int lastItemInLastRow = firstRow[lastRow.length - 1];
		System.out.println(lastItemInFirstRow + lastItemInSecondRow + lastItemInLastRow );
	}

}
